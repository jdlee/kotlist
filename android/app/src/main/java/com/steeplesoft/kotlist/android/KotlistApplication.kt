package com.steeplesoft.kotlist.android

import android.app.Application
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.launch
import org.jetbrains.anko.AnkoLogger

/**
 * Created by jdlee on 12/6/2017.
 */
class KotlistApplication : Application(), AnkoLogger {

    override fun onCreate() {
        super.onCreate()
    }
}
