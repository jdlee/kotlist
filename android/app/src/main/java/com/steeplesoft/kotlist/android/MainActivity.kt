package com.steeplesoft.kotlist.android

import android.content.Context
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.View
import com.steeplesoft.kotlist.common.Item
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.launch
import org.jetbrains.anko.AnkoComponent
import org.jetbrains.anko.AnkoContext
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.listView
import org.jetbrains.anko.matchParent
import org.jetbrains.anko.verticalLayout
import org.jetbrains.anko.wrapContent

class MainActivity :
        AppCompatActivity(),
        AnkoComponent<Context>,
        AnkoLogger {
    private val repository: KotlistDataRepository = KotlistDataRepository(this)
    private var itemAdapter: KotlistAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        itemAdapter = KotlistAdapter(this, listOf<Item>())
        itemAdapter!!.handleClick = this::processClick

        setContentView(createView(AnkoContext.create(this)))

        launch(UI) {
            itemAdapter!!.updateItems(repository.getItems())
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)

        return true
    }

    override fun onRestart() {
        super.onRestart()

    }

    override fun createView(ui: AnkoContext<Context>): View = with(ui) {
        verticalLayout {
            listView {
                adapter = itemAdapter
            }.lparams(width = matchParent, height = wrapContent)
        }
    }

    private fun processClick(item : Item) {
        launch(UI) {
            // get state of check box
            repository.updateItem(item)
        }
    }
}
