package com.steeplesoft.kotlist.android

import org.jetbrains.anko.AnkoLogger
import android.content.Context
import com.steeplesoft.kotlist.common.Item
import org.jetbrains.anko.info
import org.jetbrains.anko.warn
import ru.gildor.coroutines.retrofit.ErrorResult
import ru.gildor.coroutines.retrofit.Result
import ru.gildor.coroutines.retrofit.awaitResult
import ru.gildor.coroutines.retrofit.getOrNull

class KotlistDataRepository(private val context: Context) : AnkoLogger {
    private var data: List<Item> = ArrayList()

    private val kotlistApi: KotlistApi by lazy {
        KotlistApi.create()
    }
    var onError: ((action: Error) -> Unit)? = null

    suspend fun updateItem(item: Item): Item {
        var result = item
        kotlistApi
                .updateItem(item.id, item)
                .awaitResult()
                .ifSucceeded { response ->
                    result = response
                }
                .ifError {
                    error("Error")
                }
                .ifFailed {
                    warn("Failed to get data from server")
                }
        return result
    }

    suspend fun getItems(): List<Item> {
        kotlistApi
                .getAll()
                .awaitResult()
                .ifSucceeded { items ->
                    data = items
                }
                .ifFailed {
                    warn("Failed to get data from server")
                    onError?.invoke(Error.FAILED_TO_GET_DATA)
                }
        return data
    }

    enum class Error {
        FAILED_TO_GET_DATA
    }
}

inline fun <T : Any> Result<T>.ifSucceeded(handler: (data: T) -> Unit): Result<T> {
    (this as? Result.Ok)?.getOrNull()?.let { handler(it) }
    return this
}

inline fun <T : Any> Result<T>.ifFailed(handler: () -> Unit): Result<T> {
    if (this is ErrorResult) handler()
    return this
}

inline fun <T : Any> Result<T>.ifError(handler: (code: Int) -> Unit): Result<T> {
    (this as? Result.Error)?.response?.code()?.let { handler(it) }
    return this
}

inline fun <T : Any> Result<T>.ifException(handler: (exception: Throwable) -> Unit): Result<T> {
    (this as? Result.Exception)?.exception?.let { handler(it) }
    return this
}