package com.steeplesoft.kotlist.backend

import com.steeplesoft.kotlist.common.Item
import io.ktor.application.call
import io.ktor.http.HttpStatusCode
import io.ktor.request.receive
import io.ktor.response.respond
import io.ktor.routing.Routing
import io.ktor.routing.get
import io.ktor.routing.post
import io.ktor.routing.route

fun Routing.api(database : Database) {
    apiItems(database)
}

fun Routing.apiItems(database: Database) {
    route ("items") {
        get {
            val items = database.getItems()
            call.respond(items)
        }
        post {
            call.respond(database.saveItem(call.receive<Item>()))
        }
        get("{itemId}") {
            val id = call.parameters["itemId"] ?: return@get call.respond(HttpStatusCode.BadRequest)
            call.respond(database.getItem(id))
        }
        post("{itemId}") {
            call.respond(database.saveItem(call.receive<Item>()))
        }
    }
}